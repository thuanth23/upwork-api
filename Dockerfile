FROM node:10

RUN mkdir -p /home/node/app/node_modules && chown -R node:node /home/node/app

WORKDIR /home/node/app

COPY package*.json ./

RUN apt-get update
RUN apt-get install vim -y
RUN npm install -g nodemon
RUN npm install -g typescript
COPY --chown=node:node . .

EXPOSE 3000

CMD [ "npm", "run", "start:dev" ]
