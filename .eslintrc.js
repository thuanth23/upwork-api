module.exports = {
    parser: '@typescript-eslint/parser',  // Specifies the ESLint parser
    'env': {
        'node': true,
        'browser': true,
        'es6': true,
    },
    extends: [
        'eslint:recommended',
        'plugin:@typescript-eslint/recommended',  // Uses the recommended rules from the @typescript-eslint/eslint-plugin
        'prettier/@typescript-eslint',  // Uses eslint-config-prettier to disable ESLint rules from @typescript-eslint/eslint-plugin that would conflict with prettier
        // 'plugin:prettier/recommended',  // Enables eslint-plugin-prettier and displays prettier errors as ESLint errors. Make sure this is always the last configuration in the extends array.
    ],
    parserOptions: {
        ecmaVersion: 2018,  // Allows for the parsing of modern ECMAScript features
        sourceType: 'module',  // Allows for the use of imports
    },
    rules: {
        // Place to specify ESLint rules. Can be used to overwrite rules specified from the extended configs
        // e.g. "@typescript-eslint/explicit-function-return-type": "off",
        'max-len': ['error', {'code': 160}],
        'indent': ['warn', 4, {SwitchCase: 1}],
        'prefer-const': ['warn', {
            'destructuring': 'any',
            'ignoreReadBeforeAssign': false,
        }],
        'new-cap': ['error', {'capIsNew': false, 'newIsCap': false}],
        'quotes': ['off'],
        'space-before-function-paren': ['error', {
            'anonymous': 'always',
            'named': 'never',
            'asyncArrow': 'always',
        }],
        'no-unused-vars': 'warn',
        'no-console': 'off',
        '@typescript-eslint/explicit-function-return-type': {allowExpressions: true},
        '@typescript-eslint/explicit-member-accessibility': {
            accessibility: 'explicit',
            overrides: {
                accessors: 'explicit',
                constructors: 'no-public',
                methods: 'explicit',
                properties: 'off',
                parameterProperties: 'explicit',
            },
        },
        '@typescript-eslint/no-parameter-properties': {
            allows: [
                'public readonly',
                'protected readonly',
                'private readonly',
            ],
        },
        '@typescript-eslint/no-explicit-any': false
        // Config for extends 'plugin:prettier/recommended'
        // 'prettier/prettier': {
        //     bracketSpacing: false,
        //     printWidth: 160,
        //     tabWidth: 4,
        //     useTabs: true
        // },
    },
};
