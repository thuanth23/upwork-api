import { Injectable, NestMiddleware, HttpException, HttpStatus } from '@nestjs/common';
import { Request, Response } from 'express';
import {InjectModel} from '@nestjs/mongoose';
import {Model} from 'mongoose';
import { User } from '../src/users/interfaces/user.interface';
import * as jwt from 'jsonwebtoken';

@Injectable()
export class AuthMiddleware implements NestMiddleware{
    constructor(
        @InjectModel('users') private readonly userModel: Model<User>,
    ) {}
    use(req: Request, res: Response, next: Function) {
        const [type, token] = req.headers.authorization.split(' ');
        if (req.headers.authorization && type === 'Bearer') {
            jwt.verify(token, 'secretKey', async (err, data: {id: string}) => {
                if (err) {
                    return res.status(HttpStatus.UNAUTHORIZED).json(err);
                }
                const user = await this.userModel.findOne({_id: data.id}, {password: false});
                req.app.set('user', user);
                return next();
            });
        } else {
            return res.status(HttpStatus.UNAUTHORIZED).json('invalid token.');
        }
    }
}