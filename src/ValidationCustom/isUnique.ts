import {ValidatorConstraint, ValidatorConstraintInterface, ValidationArguments} from "class-validator";
import {Inject, Injectable} from "@nestjs/common";
import {ValidateService} from "./services/validate.service";


@Injectable()
@ValidatorConstraint({ name: "unique", async: true })
export class isUnique implements ValidatorConstraintInterface {
  constructor(
    private validateService: ValidateService
  ) {
  }

  async validate(value: string, args: ValidationArguments) {
    let field = args.constraints[0];
    let collection = args.constraints[1];
    let condition = {};
    condition[field] = value;
    console.log(213);
    let count = await this.validateService.checkUnique(condition, collection);
    console.log(count);
    return !(count > 0);

    // return false if error, true if pass
  }

  defaultMessage(args: ValidationArguments) {
    return args.constraints[0] +  ' has already taken';
  }
}
