import { Module } from '@nestjs/common';
import {ValidateService} from "./services/validate.service";
import {isUnique} from "./isUnique";

@Module({
  imports: [

  ],
  providers: [ValidateService, isUnique],
  controllers: [],
  exports: [ValidateService, isUnique],
})
export class validationCustomModule { }
