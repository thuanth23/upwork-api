import * as crypto from 'crypto';
import { Injectable, HttpException } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { User } from '../users/interfaces/user.interface';
import { JwtService } from '@nestjs/jwt';


@Injectable()
export class AuthService {
    constructor(
        private readonly jwtService: JwtService,
        @InjectModel('users') private readonly userModel: Model<User>, 
    ) {}

    async login(data) {
        const user = await this.userModel.findOne({email: data.email});
        
        if (!user) {
            throw new HttpException('Không tồn tại email này', 401);
        }
        
        let checkPassword = false;
        var encodePassword = crypto.createHmac('sha256', data.password).digest('hex');
        if (encodePassword === user.password) {
            checkPassword = true;
        }

        if (!checkPassword) {
            throw new HttpException('Email or Password không chính xác', 401);
        }

        return this.createToken(user);

    }

    async validateUser(payload: User): Promise<any> {
        // put some validation logic here
        // for example query user by id/email/username
        return {};
    }

    createToken(user) {
        const dataUser = {
            id: user.id,
            username: user.username,
            email: user.email,
            firstName: user.firstName,
            lastName: user.lastName,
            country: user.country,
            avatar: user.avatar,
            type: user.type,
            created_at: user.created_at,
            updated_at: user.updated_at,
        }

        const token = this.jwtService.sign(dataUser);
        return {
            expiresIn: 3600,
            token,
        };
    }
}
