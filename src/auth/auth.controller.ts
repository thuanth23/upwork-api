import { Controller, Post, Response, Body, HttpStatus, Get} from '@nestjs/common';
import { AuthService } from './auth.service';
import { loginDto } from './dto/auth.dto';


@Controller('auth')
export class AuthController {
    constructor(private readonly AuthService: AuthService) {}

    @Post('login')
    public async login(
        @Response() res,
        @Body() data: loginDto) {
        const auth = await this.AuthService.login(data);
        res.status(HttpStatus.OK).json(auth).send();
    }

    @Get('test')
    public async testlogin(){
        return 'login thanh cong';
    }

}


