import { IsNotEmpty, IsEmail, MinLength, MaxLength, ValidationArguments } from 'class-validator';

export class loginDto{
    @MinLength(8,{
        message : (args: ValidationArguments) => {
            return "Too short, minimum length is " + args.constraints[0] + " characters";
        } 
    })
    @MaxLength(20,{
        message : (args: ValidationArguments) => {
            return "Too long, maximum length is " + args.constraints[0] + " characters";
        }
    })
    @IsNotEmpty({
        message: 'Email is not null'
    })
    @IsEmail({},{
        message : (args: ValidationArguments) => {
            return 'The format of the email : ' + args.value + ' is incorrect';
        }
        // message: 'The format of the email is incorrect'
    })
    email: string;

    @MinLength(8,{
        message : (args: ValidationArguments) => {
            return "Too short, minimum length is " + args.constraints[0] + " characters";
        } 
    })
    @MaxLength(20,{
        message : (args: ValidationArguments) => {
            return "Too long, maximum length is " + args.constraints[0] + " characters";
        }
    })
    @IsNotEmpty({
        message: 'Password is not null'
    })
    password: string;
}
