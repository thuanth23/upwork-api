import { IsNotEmpty, IsEmail, Length } from "class-validator";
import {isUnique } from '../../ValidationCustom/isUnique';

export class registerUserDto{
  @IsNotEmpty()
  readonly firstName: string;
  @IsNotEmpty()
  readonly lastName: string;
  @IsNotEmpty()
  @Length(8)
  readonly password: string;
  @IsNotEmpty()
  @IsEmail()
  //@Min(6)
  readonly email: string;
  @IsNotEmpty()
  readonly country: string;
  @IsNotEmpty()
  readonly type: number;
  @IsNotEmpty()
  //@Validate(isUnique, ["username", 'User'])
  readonly username: string;
  readonly avatar: string;
}
