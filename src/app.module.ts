import { Module, NestModule, MiddlewareConsumer, RequestMethod } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';
import { CustomConfig } from './utils/custom.configs';
import { MongooseModule } from '@nestjs/mongoose';
import { UsersModule } from './users/users.module';
import { AuthMiddleware } from '../middlewares/auth.middleware';
import { AuthController } from './auth/auth.controller';

import {validationCustomModule} from './ValidationCustom/validationCustom.modules';

@Module({
  imports: [
      AuthModule,
      MongooseModule.forRoot(`${CustomConfig.db.URI}/${CustomConfig.db.NAME}`),
      UsersModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule implements NestModule{
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(AuthMiddleware)
      .exclude(
        { path: 'auth/login', method: RequestMethod.POST }
      )
      .forRoutes(AuthController);

  }
}
