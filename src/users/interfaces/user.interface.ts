import { Document } from 'mongoose';

export interface User extends Document {
  readonly firstName: string;
  readonly lastName: string;
  readonly username: string;
  readonly country: string;
  password: string;
  readonly email: string;
  avatar: string;
  readonly type: number;
  created_at: String;
  updated_at: String;
  deleted_at: String;
}
