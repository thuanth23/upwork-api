import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { User } from './interfaces/user.interface';
import { registerUserDto } from '../auth/dto/register.dto';

@Injectable()
export class UsersService {
    constructor(@InjectModel('User') private readonly userModel: Model<User>) {}

    async create(createUserDto: registerUserDto) {
      const createdUser = new this.userModel(createUserDto);
      if (!createdUser.avatar) {
        createdUser.avatar = '/assets/imgs/user.png';
      }
      return await createdUser.save();
    }
}
