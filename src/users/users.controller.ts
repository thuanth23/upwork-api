import { Body, Controller, Post } from '@nestjs/common';
import { UsersService } from './users.service';
import { registerUserDto } from '../auth/dto/register.dto';

@Controller('register')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Post()
  async create(@Body() createUserDto: registerUserDto) {
      return this.usersService.create(createUserDto);
  }
}
