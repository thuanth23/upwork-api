import * as mongoose from 'mongoose';
import * as crypto from 'crypto';
import { User } from '../interfaces/user.interface';

let dateformat = require('dateformat');

export const UserSchema = new mongoose.Schema({
    email: {
      type: String,
      match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/, 'Please fill a valid email address'],
     // required: true
    },
    firstName: { type: String, required: true },
    lastName: { type: String, required: true },
    country: { type: String, required: true },
    password: { type: String, required: true },
    avatar: String,
    type: Number,
    username: {
      type: String,
      required: function() {
        return this.type == 1;
      },
    },
    created_at: {
      type: Date,
      default: Date.now(),
      get: v => {
        return v === null ? '' : dateformat(new Date(v), 'yyyy/mm/dd HH:MM:ss');
      },
    },
    updated_at: {
      type: Date,
      default: Date.now(),
      get: v => {
        return v === null ? '' : dateformat(new Date(v), 'yyyy/mm/dd HH:MM:ss');
      },
    },
    deleted_at: {
      type: Date,
      default: null,
      get: v => {
        return v === null ? '' : dateformat(new Date(v), 'yyyy/mm/dd HH:MM:ss');
      },
    },
  },
  {
    versionKey: false,
  });

UserSchema.set('toJSON', {getters: true});

UserSchema.pre<User>('save', function(next) {
  var user = this;
  // only hash the password if it has been modified (or is new)
  if (!user.isModified('password')) return next();
  user.password = crypto.createHmac('sha256', user.password).digest('hex');
  const now = Date.now;
  this.created_at = now.toString();
  this.updated_at = now.toString();
  this.deleted_at = null;
  next();
});

UserSchema.pre<User>('update', function(next) {
  next();
});
