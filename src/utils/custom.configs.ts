import * as dotenv from 'dotenv';

dotenv.config();
let path;
switch (process.env.NODE_ENV) {
    case 'test':
        path = `${__dirname}./../../.env`;
        break;
    case 'production':
        path = `${__dirname}/../../.env.production`;
        break;
    default:
        path = `${__dirname}/../../.env.development`;
}
dotenv.config({path: path});

export const CustomConfig = {
    db: {
        URI: process.env.DB_URI,
        NAME: process.env.DB_NAME,
        USER: process.env.DB_USER,
        PASS: process.env.DB_PASS,
        TIMES: process.env.DB_RECONNECT_TIMES,
        TIMEOUT: process.env.DB_CONNECT_TIMEOUT,
        INTERVAL: process.env.DB_RECONNECT_INTERVAL,
    },
    server: {
        PORT: process.env.SERVER_PORT,
        SECRET_KEY: process.env.SECRET_KEY_JWT,
        PWD_SECRET: process.env.PWD_SECRET
    },
    log: {
        LOG_PATH: process.env.LOG_FILE_PATH,
    },
    statusCode: {
        SUCCESS: process.env.CODE_SUCCESS,
        FAIL: process.env.CODE_FAIL,
        NOT_EXIST: process.env.CODE_NOT_EXIST,
    },
};
